SPEAD Parameters
================

SPEAD Header
------------------
The SPEAD header has a size of 64 bits and contains information to identify the SPEAD flavour and its attributes and item pointers after the header. This information is identical for both bit depth.

+-------------------------------+--------------------------------------+
| Byte                          | Value                                |
+===============================+======================================+
| Magic Number(8 b)             | *0x53*                               |
+-------------------------------+--------------------------------------+
| SPEAD Version(8 b)            | 4                                    |
+-------------------------------+--------------------------------------+
| item id width (bytes) (8 b)   | (64-48/8) = 2                        |
+-------------------------------+--------------------------------------+
| heap address width in bytes   | 48/8=6                               |
| (8 b)                         |                                      |
+-------------------------------+--------------------------------------+
| <reserved> (16 b)             | 0                                    |
+-------------------------------+--------------------------------------+
| number of item pointers after | 8                                    |
| header (16 b)                 |                                      |
+-------------------------------+--------------------------------------+

+-------------------+-------------------------+------------------------+
|                   | **2 GHz @ 8 bits**      | **1.3 GHz @ 12 bits**  |
+===================+=========================+========================+
| SPEAD Flavor      | *SPEAD-64-48*           | *SPEAD-64-48*          |
+-------------------+-------------------------+------------------------+
| SPEAD direct      | 0x1600,0x3101,0x3102    | 0x1600,0x3101,0x3102   |
| address IDs       |                         |                        |
+-------------------+-------------------------+------------------------+
| SPEAD indirect    | 0x3310                  | 0x3311                 |
| address IDs       |                         |                        |
+-------------------+-------------------------+------------------------+
| SPEAD Version     | 4                       | 4                      |
+-------------------+-------------------------+------------------------+
| SPEAD Heap id     | SPEADTIME               | SPEADTIME (47b) + Pol  |
| (48b)             |                         | Id (1b)                |
+-------------------+-------------------------+------------------------+
| SPEAD Heap Size   | 4096 bytes              | 6144 bytes             |
| (48b)             |                         |                        |
+-------------------+-------------------------+------------------------+
| SPEAD Payload     | 4096 bytes              | 6144 bytes             |
| Length (48b)      |                         |                        |
+-------------------+-------------------------+------------------------+
| SPEAD Heap Offset | 0                       | 0                      |
| (48b)             |                         |                        |
+-------------------+-------------------------+------------------------+
| 0x1600            | SPEADTIME               | SPEADTIME              |
+-------------------+-------------------------+------------------------+
| 0x3101            | [digitiser_serial(24b)  | [digitiser_serial(24b) |
|                   | digitiser_type(8b)      | digitiser_type(8b)     |
|                   | receptor_id(14b)        | receptor_id(14b)       |
|                   | pol_id(2b)]             | pol_id(2b)]            |
+-------------------+-------------------------+------------------------+
| 0x3102            | [adc_count(16b) zeros   | [adc_count(16b) zeros  |
|                   | (30b)                   | (30b)                  |
|                   | adc_saturation_flag(1b) | a                      |
|                   | ndiode_flag(1b)]        | dc_saturation_flag(1b) |
|                   |                         | ndiode_flag(1b)]       |
+-------------------+-------------------------+------------------------+
| 0x3310 (8 bit) /  | 0                       | 0                      |
| 0x3311 (12 bit)   |                         |                        |
+-------------------+-------------------------+------------------------+

*Table 5: SPEAD-64-48 Structure*




Item Pointers
------------------
The current version comprises eight item pointers, which are located after the header. The format of the item pointers in SPEAD-64-48 and the item pointers themselves are presented in Table 7.

+-------+-----------+------------+---------------+--------------------+
| Item  | Item      | MSB(1b)    | (15b)         | LSB (48b)          |
|       | Pointer   |            |               |                    |
| Po    |           |            |               |                    |
| inter |           |            |               |                    |
| #     |           |            |               |                    |
+=======+===========+============+===============+====================+
| -     | -         | ItemA      | I             | ItemAddress        |
|       |           | ddressMode | temIdentifier |                    |
+-------+-----------+------------+---------------+--------------------+
| 1     | heap_id   | 1          | 000 0000 0000 | SPEADTIME (47b) +  |
|       |           |            | 0001          | Pol Id (1b)        |
+-------+-----------+------------+---------------+--------------------+
| 2     | heap_size | 1          | 000 0000 0000 | 4096 (8 bit) /     |
|       |           |            | 0010          | 6144 (12 bit)      |
+-------+-----------+------------+---------------+--------------------+
| 3     | he        | 1          | 000 0000 0000 | 0                  |
|       | ap_offset |            | 0011          |                    |
+-------+-----------+------------+---------------+--------------------+
| 4     | payload   | 1          | 000 0000 0000 | 4096 (8 bit) /     |
|       | \_length  |            | 0100          | 6144 (12 bit)      |
+-------+-----------+------------+---------------+--------------------+
| 5     | 0x1600    | 1          | 001 0110 0000 | SPEADTIME          |
|       |           |            | 0000          |                    |
+-------+-----------+------------+---------------+--------------------+
| 6     | 0x3101    | 1          | 011 0001 0000 | [dig               |
|       |           |            | 0001          | itiser_serial(24b) |
|       |           |            |               | digitiser_type(8b) |
|       |           |            |               | receptor_id(14b)   |
|       |           |            |               | pol_id(2b) ]       |
+-------+-----------+------------+---------------+--------------------+
| 7     | 0x3102    | 1          | 011 0001 0000 | [ adc_count(16b)   |
|       |           |            | 0010          |                    |
|       |           |            |               | zeros (30b)        |
|       |           |            |               | adc_s              |
|       |           |            |               | aturation_flag(1b) |
|       |           |            |               | ndiode_flag(1b) ]  |
+-------+-----------+------------+---------------+--------------------+
| 8     | 0x33      | 0          | 011 0011 0000 | 0                  |
|       | 10/0x3311 |            | 0000          |                    |
+-------+-----------+------------+---------------+--------------------+

*Table 6: SPEAD-64-48 Item Pointers*

Payload Data
------------------
The SPEAD payload should contain 8-bit or 12-bit signed analogue-to-digital converter (ADC) samples, with the oldest ADC sample located at the most significant bit (MSB) position of the 64-bit word and packed contiguously across 64-bit boundaries. An illustrative example of the SPEAD payload word is provided in Table 8.

+------------------+--------------------------+------------------------+
| **SPEAD PACKET** | **2 GHz @ 8 bits**       | **1.3 GHz @ 12 bits**  |
+==================+==========================+========================+
| 9-521/777        | Payload Word 1,          | Payload Word 1,        |
|                  |                          |                        |
|                  | [63:56] – ADC Sample 1   | [63:52] – ADC Sample 1 |
|                  | [7:0],                   | [11:0],                |
|                  |                          |                        |
|                  | [55:48] – ADC Sample 2   | [51:40] – ADC Sample 2 |
|                  | [7:0],                   | [11:0],                |
|                  |                          |                        |
|                  | [47:40] – ADC Sample 3   | [39:28] – ADC Sample 3 |
|                  | [7:0],                   | [11:0],                |
|                  |                          |                        |
|                  | [39:32] – ADC Sample 4   | [27:16] – ADC Sample 4 |
|                  | [7:0],                   | [11:0],                |
|                  |                          |                        |
|                  | [31:24] – ADC Sample 5   | [15:4] – ADC Sample 5  |
|                  | [7:0],                   | [11:0],                |
|                  |                          |                        |
|                  | [23:16] – ADC Sample 6   | [3:0] – ADC Sample 6   |
|                  | [7:0],                   | [11:8],                |
|                  |                          |                        |
|                  | [15:8] – ADC Sample 7    | Payload word 2,        |
|                  | [7:0],                   |                        |
|                  |                          | [63:56] – ADC Sample 8 |
|                  | [7:0] – ADC Sample 8     | [7:0],                 |
|                  | [7:0],                   |                        |
|                  |                          | …                      |
|                  | …                        |                        |
+------------------+--------------------------+------------------------+

*Table 7: SPEAD-64-48 Payload Structure*


Noise Diode Status
------------------

The SPEAD metadata header shall include a single-bit field to indicate the status of the noise diode switch at the time of the first ADC sample in the data payload.

+-----------------------+----------------------------------------------+
| **Parameter Value**   | **Description**                              |
+=======================+==============================================+
| 0                     | Noise Diode Off                              |
+-----------------------+----------------------------------------------+
| 1                     | Noise Diode On                               |
+-----------------------+----------------------------------------------+


Digitizer / Packetizer Serial Number
------------------

TBC.


Digitizer / Packetizer Receptor ID
------------------

TBC.


Digitizer / Packetizer Type
------------------

+-----------------------+----------------------------------------------+
| **Parameter Value**   | **Description**                              |
+=======================+==============================================+
| 0                     | 2.0 GHz Mode                                 |
+-----------------------+----------------------------------------------+
| 1                     | 1.3 GHz Mode                                 |
+-----------------------+----------------------------------------------+
| 2-255                 | Reserved                                     |
+-----------------------+----------------------------------------------+


Polarization ID
------------------
+-----------------------+----------------------------------------------+
| **Parameter Value**   | **Description**                              |
+=======================+==============================================+
| 0                     | Vertical Polarization                        |
+-----------------------+----------------------------------------------+
| 1                     | Horizontal Polarization                      |
+-----------------------+----------------------------------------------+
| 2-3                   | Reserved                                     |
+-----------------------+----------------------------------------------+


ADC Saturation Flag
------------------
The saturation flag should be set for a given packet if the ADC counts
are saturated.


Packetizer SPEAD Specifications
------------------
The SPEAD specifications will differ for the planned two modes of
operation. Details of SPEAD Flavor for the two modes are summarized
below.




