Digitizer Sensor List
============

Regular Digitizer Sensors
--------------------



.. admonition:: edd.digitizer.noiseSource
   :class: sensor-box

   **Description:** getting noise diode enabled/disabled
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'Off'
     - 'On'
     - 'failed'



.. admonition:: edd.digitizer.serial-number
   :class: sensor-box

   **Description:** Digitizer Board ID
   
   **Unit:** NONE
   
   **Type:** integer



.. admonition:: edd.digitizer.is-available
   :class: sensor-box

   **Description:** Boolean. True if digitizer is available.
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.digitizer.bgCalibrationEnabled
   :class: sensor-box

   **Description:** Boolean. True if ADC background calibration is enabled.
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'



.. admonition:: edd.digitizer.centre-frequency
   :class: sensor-box

   **Description:** RF Centre frequency of the active band
   
   **Unit:** Hz
   
   **Type:** float

.. admonition:: edd.digitizer.center-frequency
   :class: sensor-box

   **Description:** Double Centre Frequency
   
   **Unit:** Hz
   
   **Type:** float


.. admonition:: edd.digitizer.noise-diode
   :class: sensor-box

   **Description:** Current duty cycle of noise diode
   
   **Unit:** NONE
   
   **Type:** float
   
   **Possible state / range:**
     - '0' to '1'

.. admonition:: edd.digitizer.sampling-rate
   :class: sensor-box

   **Description:** Analog to digital conversion rate
   
   **Unit:** Hz
   
   **Type:** float
   
   **Possible state / range:**
     - '2.14748e+09'


Housekeeping Sensors
--------------------
.. admonition:: edd.digitizer.housekeeping.temperature.fpga
   :class: sensor-box

   **Description:** Digitizer FPGA Core Temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.digitizer.housekeeping.temperature.board
   :class: sensor-box

   **Description:** digitizer board temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.digitizer.housekeeping.temperature.adc0
   :class: sensor-box

   **Description:** digitizer adc0 core temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.digitizer.housekeeping.temperature.adc1
   :class: sensor-box

   **Description:** digitizer adc1 temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.digitizer.housekeeping.temperature.ffly0
   :class: sensor-box

   **Description:** digitizer FireFly-0 temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '60'
     - '0'
     - '70'

.. admonition:: edd.digitizer.housekeeping.temperature.ffly1
   :class: sensor-box

   **Description:** digitizer FireFly-1 temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '60'
     - '0'
     - '70'

.. admonition:: edd.digitizer.housekeeping.current.board
   :class: sensor-box

   **Description:** Digitizer Board Current
   
   **Unit:** A
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '2'
     - '0'
     - '2'

.. admonition:: edd.digitizer.housekeeping.voltage.int
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '1'
     - '0'
     - '1'

.. admonition:: edd.digitizer.housekeeping.voltage.aux
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

.. admonition:: edd.digitizer.housekeeping.temperature.4644
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '10'
     - '90'
     - '5'
     - '95'

.. admonition:: edd.digitizer.housekeeping.voltage.vcc
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '6'
     - '10'
     - '5'
     - '10'


.. admonition:: edd.digitizer.housekeeping.voltage.3V3_synth
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '3'
     - '3'
     - '2'
     - '3'

.. admonition:: edd.digitizer.housekeeping.voltage.2V5_synth
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '2'
     - '2'
     - '2'
     - '2'

.. admonition:: edd.digitizer.housekeeping.voltage.1V2_adcd
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

.. admonition:: edd.digitizer.housekeeping.voltage.3V3
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '3'
     - '3'
     - '2'
     - '3'

.. admonition:: edd.digitizer.housekeeping.voltage.3V3_ffly
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '3'
     - '3'
     - '2'
     - '3'

.. admonition:: edd.digitizer.housekeeping.voltage.1V9_adc
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '2'

.. admonition:: edd.digitizer.housekeeping.voltage.1V2_adc_a
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

