Operation Example
==========================

Introduction
------------

This guide provides detailed instructions on how to configure the packetizer using the provided script. The configuration process involves setting up the packetizer's parameters, managing sensor data, and controlling data capture and streaming. The packetizer is controlled via the KATCP protocol, and this documentation outlines the sequence of commands and their purposes.

Initialization and Setup
------------------------

1. **Import Necessary Modules and Setup Logging:**

   Import the required Python modules. Initialize the logging system.

   .. code-block:: python

      from concurrent.futures import ProcessPoolExecutor
      import asyncio
      import time
      import aiokatcp
      import redis
      import json
      from aiokatcp.sensor import Sensor
      from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change, getArgumentParser
      from mpikat.core.digpack_client import DigitiserPacketiserClient
      from mpikat.core.datastore import EDDDataStore
      from mpikat.core import logger as logging
      from mpikat.utils.data_streams import PacketizerStreamFormat

      _log = logging.getLogger("mpikat.pipelines.digpack_controller")

2. **Define Default Configuration:**

   Set default values for various packetizer parameters.

   .. code-block:: python

      _DEFAULT_CONFIG = {
          "id": "DigitizerController",
          "type": "DigitizerController",
          "bit_depth": 12,
          "sampling_rate": 2600000000,
          "predecimation_factor": 1,
          "flip_spectrum": False,
          "invert_1pps": False,
          'sync_time': 0,
          'noise_diode_sync_window': 5,
          'noise_diode_pattern': {"percentage": 0, "period": 0},
          'force_reconfigure': False,
          'skip_packetizer_config': False,
          'packer_sensor_update_interval': 10,
          'dummy_configure': False,
          'max_sync_age': 82800,
          'interface_addresses': [],
          'interface_mac': [],
          'digital_filter': 0,
          'digpack_generation': 1,
          'snapshot_frequency': -1.,
          "output_data_streams": {
              "polarization_0": {
                  "format": "MPIFR_EDD_Packetizer:1",
                  "polarization": 0,
              },
              "polarization_1": {
                  "format": "MPIFR_EDD_Packetizer:1",
                  "polarization": 1,
              }
          }
      }

Configuration Process
---------------------

3. **Connecting to the Packetizer:**

   Establish a connection to the packetizer control interface using KATCP.

   .. code-block:: python

      self._client_connection = aiokatcp.Client(packetizer_ip, packetizer_port)

4. **Initial Setup in ``configure`` Method:**

   Install sensors and update sensor values.

   .. code-block:: python

      await self.install_sensors()
      await self.packer_sensor_update()

5. **Query Packer Command Prefix:**

   Retrieve the packer command prefix for subsequent commands.

   .. code-block:: python

      await self._client.query_packer_command_prefix()

6. **Stop Current Capture:**

   Ensure that any ongoing data capture is stopped before reconfiguring.

   .. code-block:: python

      await self._client.capture_stop()

7. **Set Packetizer Parameters:**

   Configure various packetizer settings including sampling rate, predecimation factor, spectrum flipping, bit width, and digital filter.

   .. code-block:: python

      await self._client.set_sampling_rate(self._config["sampling_rate"])
      await self._client.set_predecimation(self._config["predecimation_factor"])
      await self._client.flip_spectrum(self._config["flip_spectrum"])
      await self._client.set_bit_width(self._config["bit_depth"])
      await self._client.set_digital_filter(self._config['digital_filter'])

8. **Set Network Interfaces:**

   Assign IP and MAC addresses to the digitizer NICs.

   .. code-block:: python

      for i, ip_address in enumerate(self._config["interface_addresses"]):
          await self._client.set_interface_address(i, ip_address)
      for i, mac in enumerate(self._config["interface_mac"]):
          await self._client.set_mac_address(i, mac)

9. **Set Data Destinations:**

   Configure the destinations for data streams.

   .. code-block:: python

      await self._client.set_destinations(vips, hips)

10. **Invert 1PPS:**

    Invert the 1PPS sampling sense if required.

    .. code-block:: python

       await self._client.invert_1pps(self._config["invert_1pps"])

11. **Synchronize Packetizer:**

    Synchronize the packetizer using the provided sync time.

    .. code-block:: python

       if self._config["sync_time"] > 0:
           await self._client.synchronize(self._config["sync_time"])
       else:
           await self._client.synchronize()

12. **Update Output Data Streams:**

    Set sync time and other parameters for output data streams.

    .. code-block:: python

       sync_time = await self._client.get_sync_time()
       for pol in ["polarization_0", "polarization_1"]:
           self._config["output_data_streams"][pol]["sync_time"] = sync_time
           self._config["output_data_streams"][pol]["bit_depth"] = self._config["bit_depth"]
           self._config["output_data_streams"][pol]["sample_rate"] = self._config["sampling_rate"] / self._config['predecimation_factor']

Data Capture and Streaming
--------------------------

13. **Start Data Capture:**

    Begin streaming spectrometer output.

    .. code-block:: python

       await self._client.capture_start()

14. **Stop Data Capture:**

    Stop streaming data during deconfiguration.

    .. code-block:: python

       await self._client.capture_stop()

Snapshot Handling
-----------------

15. **Enable Snapshot:**

    Enable and periodically plot packetizer snapshots.

    .. code-block:: python

       await self._client.enable_snapshot()
       data = await self._client.get_snapshot()

16. **Set Noise Diode Pattern:**

    Configure the noise diode firing pattern.

    .. code-block:: python

       task = self._client.set_noise_diode_firing_pattern(c["percentage"], c["period"], starttime=sync_time)

Main Execution
--------------

17. **Run the Pipeline Server:**

    Parse arguments, connect to the packetizer, and run the pipeline server.

    .. code-block:: python

       def main():
           parser = getArgumentParser()
           parser.add_argument('--packetizer-ip', dest='packetizer_ip', type=str, help='The control IP of the packetizer')
           parser.add_argument('--packetizer-port', dest='packetizer_port', type=int, default=7147, help='The port number to control the packetizer')
           args = parser.parse_args()

           _log.info('Connecting to packetizer @ {}:{}'.format(args.packetizer_ip, args.packetizer_port))

           async def wrapper():
               pipeline = DigitizerControllerPipeline(args.host, args.port, args.packetizer_ip, args.packetizer_port)
               await launchPipelineServer(pipeline, args)
           loop = asyncio.get_event_loop()
           loop.run_until_complete(wrapper())

       if __name__ == "__main__":
           main()

Conclusion
----------

By following these steps, you can configure the packetizer using the provided script. This guide covers the essential commands and processes required to set up, manage, and control the packetizer effectively. If you encounter any issues or need further assistance, please refer to the script comments and logging outputs for additional information.

