KATCP command List
=================

The KATCP (Karoo Array Telescope Control Protocol) command list is an extensive collection of operations used to manage and control various aspects of the EDD (Effelsberg Direct Digitization) packetizer and other related components in a system. This session categorizes and describes the available commands to streamline interactions and ensure efficient handling of data streams, diagnostics, debugging, and miscellaneous system operations.

Each command is tailored to perform specific tasks, such as configuring stream identifiers, adjusting packetizer settings, synchronizing data, and managing network interfaces. This structured list allows users to quickly identify and utilize the necessary commands for their specific requirements. Below are the categorized operations available within the KATCP command list.


EDD Packetizer Stream Operations
----------------------------

- **edd-packetizer-stream-identifier-set** - Sets a magic number to identify the data stream.
  - Parameters: <IFACE> <UINT30>

EDD Packetizer Snapshot Operations
----------------------------

- **edd-packetizer-snapshot-postffts-ncutbits** - Sets the number of bits to cut from packetizer data.
- **edd-packetizer-snapshot-postffts-enable** - Enables FFTS-Snapshot capturing after filtering.
  - Parameters: 1|0|true|false

EDD Packetizer SPROC Operations
----------------------------

- **edd-packetizer-sproc-deskew-get** - Gets deskew in counts of sample periods for vertical and horizontal.
- **edd-packetizer-sproc-deskew-set** - Sets deskew in counts of sample periods for vertical and horizontal.
  - Parameters: [vert horr]
- **edd-packetizer-sproc-get-zero-time** - Gets the base time of the synchronized data stream.
- **edd-packetizer-sproc-use-1pps-set** - Sets whether to use 1pps.
  - Parameters: [1 \| 0]
- **edd-packetizer-sproc-use-1pps-get** - Gets whether 1pps is being used.
  - Parameters: 1|0
- **edd-packetizer-sproc-sync-timestamp-set** - Sets the sync timestamp.
- **edd-packetizer-sproc-sync-timestamp-get** - Gets the sync timestamp.
- **edd-packetizer-sproc-filter-selection-set** - Sets filter selection.
  - Parameters: [U4]
- **edd-packetizer-sproc-filter-selection-get** - Gets filter selection.
  - Parameters: U4
- **edd-packetizer-sproc-datasize-set** - Sets the data size.
- **edd-packetizer-sproc-datasize-get** - Gets the data size.
- **edd-packetizer-sproc-data2-enable-set** - Sets whether data2 is enabled.
  - Parameters: [1|0]
- **edd-packetizer-sproc-data2-enable-get** - Gets whether data2 is enabled.
  - Parameters: 1|0
- **edd-packetizer-sproc-data1-enable-set** - Sets whether data1 is enabled.
  - Parameters: [1|0]
- **edd-packetizer-sproc-data1-enable-get** - Gets whether data1 is enabled.
  - Parameters: 1|0
- **edd-packetizer-sproc-predecimation** - Sets pre-decimation.
  - Parameters: Factor U4 (1..15)
- **edd-packetizer-sproc-flipsignalspectrum** - Enables/Disables flipped signal spectrum transmission.
  - Arguments: 1, 0, on, off, true, false
- **edd-packetizer-sproc-showmode** - Shows the current EDD-Transmission Mode ID.
  - Answers: edd08, edd10, edd12, UNDEFINED
- **edd-packetizer-sproc-switchmode** - Changes the current EDD-Transmission Mode.
  - Valid arguments: edd8, edd10, edd12

EDD Packetizer DNET Operations
----------------------------

- **edd-packetizer-dnet-clear-error-leds-request** - Clears error LEDs.
- **edd-packetizer-dnet-dhcphandler-enable** - Enables/Disables the Packetizer DHCP Handler for a selected interface.
  - Args: <IFACE>, <BOOL>
- **edd-packetizer-dnet-start-stop-set** - Sets start/stop.
  - Parameters: [1|0]
- **edd-packetizer-dnet-start-stop-get** - Gets start/stop.
  - Parameters: 1|0
- **edd-packetizer-dnet-src-ip-selection-set** - Sets source IP selection.
  - Parameters: <IFACE><0|dhcp|1|static>
- **edd-packetizer-dnet-src-ip-selection-get** - Gets source IP selection.
  - Parameters: dhcp|static
- **edd-packetizer-dnet-source-port-set** - Sets the TCP port of a data stream.
- **edd-packetizer-dnet-source-port-get** - Gets the TCP port of a data stream.
- **edd-packetizer-dnet-source-mac-set** - Sets the device hardware address of a data stream.
- **edd-packetizer-dnet-source-mac-get** - Gets the device hardware address of a data stream.
- **edd-packetizer-dnet-source-ip-set** - Sets the device IP of a data stream.
- **edd-packetizer-dnet-source-ip-get** - Gets the device IP of a data stream.
- **edd-packetizer-dnet-msg-request** - Triggers a message send request.
- **edd-packetizer-dnet-msgaddress-set** - Sets message address.
  - Parameters: [first] [last]
- **edd-packetizer-dnet-msgaddress-get** - Gets message start/end address.
- **edd-packetizer-dnet-heartbeat-set** - Sets heartbeat.
  - Parameters: [1|0]
- **edd-packetizer-dnet-heartbeat-get** - Gets heartbeat.
  - Parameters: 1|0
- **edd-packetizer-dnet-dhcp-enable** - Enables/Disables DHCP at the 40GB-IFace.
  - Opts: <Stream>, <Bool>
- **edd-packetizer-dnet-dhcp-dhcpServerIp-get** - Gets the DHCP-Server that made the last offer.
- **edd-packetizer-dnet-dhcp-ip-set** - Sets the DHCP IP of a data stream.
- **edd-packetizer-dnet-dhcp-leasetime-get** - Gets the DHCP lease time of a data stream.
  - Args: <Stream>
- **edd-packetizer-dnet-dhcp-ip-get** - Gets the DHCP IP of a data stream.
  - Args: <Stream>
- **edd-packetizer-dnet-destination-port-get** - Gets the TCP port of a data stream.
- **edd-packetizer-dnet-destination-mac-get** - Gets the destination MAC of a data stream.
- **edd-packetizer-dnet-destination-ip-get** - Gets the destination IP of a data stream.

EDD Capture Operations
----------------------------

- **edd-capture-stop** - Stops transmission of a selected stream.
- **edd-capture-start** - Starts transmission of a selected stream.
  - Parameters: [STREAM] (STREAM: 0,1,v,h,V,H. Omit for starting all streams)
- **edd-capture-list** - Displays available data streams.
- **edd-capture-destination** - Sets the destination of a data stream.

EDD Operations
----------------------------

- **edd-select-sub-band** - Selects the sub-band by setting the center frequency.
  - Parameters: with center-frequency as a floating point value in MHz.
- **edd-synchronize** - Sets the synchronization time.
- **edd-noise-source** - Schedules a particular noise source pattern.
- **edd-adc-snap-shot** - Retrieves a small number of data samples.
- **hwinfo** - Provides information on supported modes and available features.
- **edd-refclk-evaluation-reset** - Resets the statistics buffer at the reference clock evaluation module.

EDD Packetizer Diagnostics Operations
----------------------------

- **edd-packetizer-diag-jesd** - Shows status about current JESD-core conditions in JSON format.
- **edd-packetizer-diag-errorFlags** - Identifies system issues as a flag list.

EDD Packetizer Debug Operations
----------------------------

- **edd-packetizer-debug-playgnd** - Playground area for testing during the development process. Use never in a productive environment.
- **edd-packetizer-debug-mtrace** - Enables allocation tracing to a file or disables tracing.
  - Parameters: [FILENAME \|\| stop]

EDD Debug Operations
----------------------------

- **edd-debug-sproc-sync-info** - Shows information about the time stamp sample counter and its time of synchronization.
- **edd-debug-sensorhandler-verbose** - Changes the verbosity of the sensor handler task.
  - Parameters: 0|1|2
- **edd-debug-reset-uart** - Sets or clears UART reset.
- **edd-debug-reset-dclk** - Sets or clears DCLK reset.
- **edd-debug-reset-qsfp** - Sets or clears QSFP reset.
- **edd-debug-reset-dsp** - Sets or clears DSP reset.
- **edd-debug-register-write** - Writes N registers.
- **edd-debug-register-read** - Reads N registers.
- **edd-debug-noisesource** - Enables/Disables the noise diode.
  - Parameters: [1|0]

EDD Packetizer JESD Operations
----------------------------

- **edd-packetizer-jesd-read** - Reads values from JESD Register Space.
  - Parameters: <ADDR> [N]

EDD Packetizer Miscellaneous Operations
----------------------------

- **edd-packetizer-fanspeed** - Switches fan speed to high or low.
- **edd-packetizer-ffly-getLosAlarmFlags** - Reads the LOS Alarm register, indicating transmitter loss for each fiber.
  - Parameters: <FFLY>
- **edd-packetizer-serialno** - Queries the serial number.
- **edd-packetizer-version-digitizer** - Shows 'digitizer-fpga' version information.
- **edd-packetizer-version-packetizer** - Shows 'packetizer-fpga' version information.
- **edd-packetizer-version-packer** - Shows 'packer' build information.
- **edd-packetizer-snapshot-resetpps** - Resets the 1pps counter and t-Zero.
- **edd-packetizer-snapshot-enable-time** - Enables Snapshot Data within SystemStatus Task.
- **edd-packetizer-snapshot-disable-time** - Disables Snapshot Data within SystemStatus Task.
- **edd-packetizer-snapshot-enable-spec** - Enables Snapshot Data within SystemStatus Task.
- **edd-packetizer-snapshot-disable-spec** - Disables Snapshot Data within SystemStatus Task.
- **edd-packetizer-snapshot-get-time** - Gets a live data time domain snapshot.
- **edd-packetizer-snapshot-get-spec** - Gets a logarithmic live data frequency-domain snapshot.
  - Opts: 'epoche', 'wait'
- **edd-packetizer-1pps-invert-disable** - Uses the falling edge of 1pps pulse as default.
- **edd-packetizer-1pps-invert-enable** - Uses the rising edge of 1pps pulse. Default is falling.
- **edd-packetizer-system-hwinfo** - Provides information on supported modes and available features.
- **edd-packetizer-system-reboot** - Forces a system reboot.
- **edd-packetizer-system-reinit** - Re-initializes the packetizer and digitizer.
  - Opts: [core [fClk [clkDelay [bgCal]]]]
- **edd-packetizer-state** - Returns error state.

Miscellaneous Operations
----------------------------

- **client-list** - Displays client list.
- **listener-create** - Accepts new duplex connections on a given interface.
  - Parameters: label, [port], [interface], [group]
- **system-info** - Reports server information.
- **version** - Manages version operations for sensors.
  - Parameters: sensor, [add module version [mode]|remove module]
- **sensor** - Manages sensor operations.
  - Parameters: sensor, [list|create|relay job-name]
- **process** - Registers a process command.
  - Parameters: executable, help-string, [mode]
- **job** - Manages job operations.
  - Parameters: job, [list|process notice-name exec://executable-file|network notice-name katcp://net-host:remote-port|watchdog job-name|match job-name inform-message|stop job-name]
- **arb** - Manages arbitrary callback manipulation.
  - Parameters: [list]
- **define** - Manages runtime definitions.
  - Parameters: [mode name]
- **notice** - Manages notice operations.
  - Parameters: notice, [list|watch|wake]
- **dispatch** - Manages dispatch operations.
  - Parameters: dispatch, [list]
