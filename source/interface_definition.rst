Introduction
================
Intended Audience
----------------

1) A developer who wishes to implement a packetizer interface to work with EDD processing backend (with mpikat). This document provides all the required information to create a functional EDD packetizer.

2) A developer who wishes to build a backend to interface with an EDD packetizer (of either the first or second generation). 


System Overview
----------------
The EDD system is split into two modules, each with distinct functions:

Digitizer Module: Converts analog signals into time-labeled digital signals.
Packetizer Module: Distributes the digital signals in a pre-defined, grouped data format.
These modules can be separated by a few hundred meters, connected via optical fiber.

Digitizer Module
-------

* Key Components:

   #. Analog Digital Converters (ADC): Two ADCs convert analog signals into digital format, synchronized with a 1 pulse-per-second (pps) signal.
   #. Synthesizer: Provides the sampling frequency based on a 100 MHz reference clock.
   #. Field-Programmable Gate Array (FPGA): Acts as a processor to manage communication, running at the reference clock rate.
   #. Optical Fiber Connector: Connects the digitizer to the packetizer.

* Functionality:

   #. The ADCs sample analog signals simultaneously.
   #. The synthesizer ensures accurate sampling frequency.
   #. The FPGA organizes the digitizer's communication.

Packetizer Module
-------

* Key Components:

   #. FPGA: Runs synchronized at an integer fraction of the ADC sampling rate, handling time labeling, arithmetic, and data stream restructuring.
   #. Raspberry Pi (RasPi): Serves as the access point, organizing operation, housekeeping, and communication.
   #. Optical Fiber Connector: Connects to the digitizer.
   #. High-Speed Ethernet Interfaces: Connect the packetizer to the network infrastructure.

Versions and Deployment
----------

* Version 1: Part of the MPIfR S-Band receiver system for the MeerKAT array, operating in MeerKAT, Effelsberg, MPG-SKA Prototype Telescope, and the Thai National Radio Telescope (TNRT).
* Version 2: Prototype system with larger bandwidth and higher data rates.

.. image:: digpack1.png

*Figure: Digitizer/Packetizer version 1*

.. image:: digpack2.png 

*Figure: Digitizer/Packetizer version 2*



SPEAD Packet Description
----------

The interface enables the transfer of data from the Packetizer to the Backends via the SPEAD protocol over Ethernet (multicast UDP stream). The Packetizer transmits data over two 40/100 Gigabit Ethernet (GbE) links to a data switch. 

The data is transmitted from the Packetizer via multicast UDP streams. It is necessary for the packetizer to be able to support the following requirements for the multicast streams in order to facilitate the capture of data by various backends over 40GbE/100GbE Ethernet.

+-------------------------+--------------------------------------------+
| Number of Phases per    | 1 - 255                                    |
| Polarization            |                                            |
|                         |                                            |
| (Multicast Streams)     |                                            |
+=========================+============================================+
| Heap Size               | 1k Bytes – 1M Bytes                        |
+-------------------------+--------------------------------------------+
| Configuration mode      | On the fly over katcp protocol             |
+-------------------------+--------------------------------------------+


Packet Timestamp
------------------

The SPEAD metadata will include a 48-bit timestamp synchronized with the first ADC sample in the data payload. The first sample needs to be aligned with the 1PPS signal available at Effelsberg. A synchronization command to sync the data with a given 1PPS signal should be available from the Packetizer interface.

The timestamp should then be incremented by the number of samples per FPGA clock available from the ADC. It should represent the number of ADC samples from the synchronization with the 1PPS signal. This timestamp is referred to as SPEADTIME for the rest of this document.

In the current setup, the SPEADTIME increments by 4096 for a given UDP packet, and the actual value is written in the SPEAD header.
 


EDD Packetizer Data Packetization
--------------

* Two versions of the digitalization modules have been development, a digitizer 1 / packetizer 1 as part of the MPIfR S-Band receiver system of the MeerKAT array, and a digitizer 2 and packetizer 2 prototype system providing larger bandwidth and higher data rates. Currently version 1 systems are operating in MeerKAT, Effelsberg, MPG-SKA Prototype Telescope, and the Thai National Radio Telescope (TNRT). 

.. list-table::
   :header-rows: 1
   :widths: 1 1 1

   * - 
     - **EDD1**
     - **EDD2**
   * - **Hardware**
     - Packetizer 1 + Digitizer 1
     - Packetizer 2 + Digitizer 2
   * - **FPGA**
     - EDD
     - EDD2
   * - **Sampling Frequency**
     - 2.6 / 3.2 / 3.5 / 3.6 / 4.0 / 4.096 GHz
     - 5.0 / 6.0 GHz
   * - **max Bandwidth**
     - < 2000 MHz
     - < 3000 MHz
   * - **Decimation Factor**
     - 1 / 2 / 4 / 8 / 16
     - 1 / 2 / 4 / 8 / 16 / 32
   * - **Sub-Band Filters**
     - none
     - none
   * - **Ethernet Interfaces**
     - 2 x 40 GbE
     - 2 x 100 GbE
   * - **max. Bitrate**
     - 2 x 35 Gbit/s
     - 2 x 96 Gbit/s
   * - **Bit Depth**
     - 8 / 10 / 12 bit/sample
     - 8 / 10 / 12 bit/sample
   * - **Polarisations**
     - 2
     - 2

*Table 1: Comparison of different digitizer / packetizer versions*

* The Packetizer can operate in two different modes, each corresponding to the number of bits per sample—8 bits or 12 bits. These modes dictate the size of the data payload and the overall structure of the data frames transmitted over a 40/100GbE link. Below is a combined summary of the two modes, followed by the detailed data frame structures for each.


+-----------------------+----------------------------------------------+----------------------------------------------+
| **ADC Data Frame**    | **Number of Bytes in 8-bit Mode**            | **Number of Bytes in 12-bit Mode**           |
+=======================+==============================================+==============================================+
| Preamble              | 7                                            | 7                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| Start of the Frame    | 1                                            | 1                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| MAC                   | 12                                           | 12                                           |
+-----------------------+----------------------------------------------+----------------------------------------------+
| Protocol              | 2                                            | 2                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| IP Header             | 20                                           | 20                                           |
+-----------------------+----------------------------------------------+----------------------------------------------+
| UDP Header            | 8                                            | 8                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| SPEAD Header          | 8 + 64                                       | 8 + 64                                       |
+-----------------------+----------------------------------------------+----------------------------------------------+
| Padding               | 6                                            | 6                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| CRC32                 | 4                                            | 4                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| Dead Time             | 2                                            | 2                                            |
+-----------------------+----------------------------------------------+----------------------------------------------+
| Data Payload          | 4096 (4096 \* 8 bits)                        | 6144 (4096 \* 12 bits)                       |
+-----------------------+----------------------------------------------+----------------------------------------------+

*Table 2 : Comparison of payload structure in 8/12 bit mode*