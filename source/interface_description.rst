Interface Description
=====================

The interface enables the transfer of data from the Packetizer to the Backends via the SPEAD protocol over Ethernet (multicast UDP stream). The Packetizer transmits data over two 40/100 Gigabit Ethernet (GbE) links to a Mellanox switch. 

The data is transmitted from the Packetizer via multicast UDP streams. It is necessary for the packetizer to be able to support the following requirements for the multicast streams in order to facilitate the capture of data by various backends over 40GbE/100GbE Ethernet.

+-------------------------+--------------------------------------------+
| Number of Phases per    | 1 - 255                                    |
| Polarization            |                                            |
|                         |                                            |
| (Multicast Streams)     |                                            |
+=========================+============================================+
| Heap Size               | 1k Bytes – 1M Bytes                        |
+-------------------------+--------------------------------------------+
| Configuration mode      | On the fly over katcp protocol             |
+-------------------------+--------------------------------------------+


