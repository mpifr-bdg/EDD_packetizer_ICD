Packetizer Sensors List
============

System Status
-------------

.. admonition:: edd.packetizer.system-state
   :class: sensor-box

   **Description:** Information hardware state.
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'basic initialization'
     - 'writing FPGA image'
     - 'initializing runtime environment'
     - 'setting default values'
     - 'initializing default values'
     - 'starting status monitor'
     - 'initializing packetizer hardware'
     - 'initializing digitizer hardware'
     - 'System initialization'
     - 'Application Environment Initialization'
     - 'Logical state, monitoring begin'
     - 'normal operation'
     - 'HelthCheck Issued Identified'
     - 'waiting for digitizer hardware'
     - 'digitizer lost'
     - 'waiting for digitizer for re-initialization'
     - 'Over Temperature Detected'
     - 'Over Temperature'
     - 'Writing Fpga image failed.'
     - 'Initializing Hardware failed'
     - 'Initializing Digitizer failed.'
     - 'Logical state, monitoring end'
     - 'Re-Initialization'
     - 'FPGA core incompatible'

.. admonition:: edd.packetizer.systemstatus.snapshot.ctr
   :class: sensor-box

   **Description:** Increments after each snapshot
   
   **Unit:** counts
   
   **Type:** integer

.. admonition:: edd.packetizer.systemstatus.rawsnapshot.ctr
   :class: sensor-box

   **Description:** Increments after each snapshot
   
   **Unit:** counts
   
   **Type:** integer

.. admonition:: edd.packetizer.systemstatus.snapshot.int-time
   :class: sensor-box

   **Description:** Increments after each snapshot
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.systemstatus.snapshot.isSpectraEnabled
   :class: sensor-box

   **Description:** Returns 1 if spectra snapshot mode is enabled, 0 if not, -1 on error.
   
   **Unit:** NONE
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '1'
     - '-1'
     - '2'

.. admonition:: edd.packetizer.systemstatus.snapshot.isTimeEnabled
   :class: sensor-box

   **Description:** Returns 1 if time snapshot mode is enabled, 0 if not, -1 on error.
   
   **Unit:** NONE
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '1'
     - '-1'
     - '2'

.. admonition:: edd.packetizer.systemstatus.snapshot.totalpwr.h
   :class: sensor-box

   **Description:** calculated Total Power Value of Horizontal Polarization
   
   **Unit:** NONE
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '1e+07'
     - '0'
     - '2.14748e+09'

.. admonition:: edd.packetizer.systemstatus.snapshot.totalpwr.v
   :class: sensor-box

   **Description:** calculated Total Power Value of Vertical Polarization
   
   **Unit:** NONE
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '1e+07'
     - '0'
     - '2.14748e+09'

.. admonition:: edd.packetizer.sideband
   :class: sensor-box

   **Description:** Which sideband is in use for the downconversion
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'upper'
     - 'lower'
     - 'unknown'
  
.. admonition:: edd.packetizer.fanspeed
   :class: sensor-box

   **Description:** Getting Fan Speed High/Low
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'Low'
     - 'High'
     - 'Failed'

.. admonition:: edd.packetizer.pkgCtr
   :class: sensor-box

   **Description:** Snapshpot capture counter
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.effectiveSampleRate
   :class: sensor-box

   **Description:** The effective sample frequency applied at the data stream
   
   **Unit:** 1/s
   
   **Type:** integer

.. admonition:: edd.packetizer.sampleFreq
   :class: sensor-box

   **Description:** Requested Sample Frequency
   
   **Unit:** MHz
   
   **Type:** integer

.. admonition:: edd.packetizer.noisediode.onfraction
   :class: sensor-box

   **Description:** Reflects the on-fraction of Noise Diode Signal
   
   **Unit:** NONE
   
   **Type:** float

.. admonition:: edd.packetizer.noisediode.dutycycle
   :class: sensor-box

   **Description:** Reflects the duty cycle of noise diode signal
   
   **Unit:** NONE
   
   **Type:** float

.. admonition:: edd.packetizer.noisediode.period
   :class: sensor-box

   **Description:** Reflects the period time of Noise Diode Signal
   
   **Unit:** s
   
   **Type:** float

.. admonition:: edd.packetizer.sampleDecimation
   :class: sensor-box

   **Description:** Reflects the sample frequency decimation factor
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.samplingDepth
   :class: sensor-box

   **Description:** Reflects the sampling bit depth of data stream
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.snapshot.post-ffts.enabled
   :class: sensor-box

   **Description:** Spectra snapshot after signal processing pipeline
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.snapshot.post-ffts.n-cut-bits
   :class: sensor-box

   **Description:** The number of bits, ignored for post-FFTs snapshots
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.sproc.selected-filter
   :class: sensor-box

   **Description:** Filter 0 - 15
   
   **Unit:** NONE
   
   **Type:** integer
   
   **Possible state / range:**
     - '0' to '15'
     - '-1' to '16'

.. admonition:: edd.packetizer.sproc.iface00.capturing
   :class: sensor-box

   **Description:** Indicates data capturing enabled/disabled of 40g-iFace0
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'Off'
     - 'On'
     - 'failed'

.. admonition:: edd.packetizer.sproc.iface01.capturing
   :class: sensor-box

   **Description:** Indicates data capturing enabled/disabled of 40g-iFace1
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'Off'
     - 'On'
     - 'failed'


Time Reference
--------------
.. admonition:: edd.packetizer.timestamp-multiplier
   :class: sensor-box

   **Description:** Number of samples that a single increment in the sample counter in the data stream actually represents. Set to 1 if there is a one-to-one correspondence.
   
   **Unit:** NONE
   
   **Type:** integer
   
   **Possible state / range:**
     - '1' to '4'
     - '0' to '5'

.. admonition:: edd.packetizer.time.synchronisation-epoch
   :class: sensor-box

   **Description:** Reports the time of last sample counter reset.
   
   **Unit:** Epoche
   
   **Type:** integer

.. admonition:: edd.packetizer.time.synchronisation-offset
   :class: sensor-box

   **Description:** Reports the correction offset of last sample counter reset.
   
   **Unit:** ns
   
   **Type:** integer
   
   **Possible state / range:**
     - '0' to '66666664'
     - '-1' to '66666668'

.. admonition:: edd.packetizer.scg.time.stamps.modulus
   :class: sensor-box

   **Description:** Delta of each tick to a stable time reference point
   
   **Unit:** Samples
   
   **Type:** integer

.. admonition:: edd.packetizer.scg.time.stamps
   :class: sensor-box

   **Description:** The sample count for the received PPS
   
   **Unit:** Samples
   
   **Type:** integer

.. admonition:: edd.packetizer.diag.refClk.jitter
   :class: sensor-box

   **Description:** RefClk Jitter. Standard deviation of sample clock error per 1PPS pulse
   
   **Unit:** N Samples
   
   **Type:** float
   
   **Possible state / range:**
     - '1' to '100'
     - '0' to '101'

.. admonition:: edd.packetizer.diag.refClk.error.average
   :class: sensor-box

   **Description:** Average on absolute sample clock error related to 1PPS pulse
   
   **Unit:** Hz
   
   **Type:** float
   
   **Possible state / range:**
     - '1' to '100'
     - '0' to '101'

.. admonition:: edd.packetizer.diag.refClk.error.absolut
   :class: sensor-box

   **Description:** Average on absolute sample clock error related to 1PPS pulse
   
   **Unit:** Hz
   
   **Type:** integer

.. admonition:: edd.packetizer.1pps.length
   :class: sensor-box

   **Description:** The Measured 1pps Phase time
   
   **Unit:** us
   
   **Type:** integer
   
   **Possible state / range:**
     - '999985'
     - '1000015'
     - '999970'
     - '1000030'

.. admonition:: edd.packetizer.1pps.last
   :class: sensor-box

   **Description:** Time/us since last occurrence of a 1pps pulse
   
   **Unit:** us
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '1000100'
     - '-1'
     - '2000000'

.. admonition:: edd.packetizer.1pps.isInverted
   :class: sensor-box

   **Description:** if 1: Trigger on rising edge of 1pps pulse. Default is falling.
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.clockoffset
   :class: sensor-box

   **Description:** offset between 1pps and system clock
   
   **Unit:** us
   
   **Type:** integer
   
   **Possible state / range:**
     - '-1250000'
     - '250000'
     - '-500000'
     - '500000'


Network Interface
-----------------
.. admonition:: edd.packetizer.dnet.iface00.am-lock-status
   :class: sensor-box

   **Description:** QSFP Alignment Marker Lock status for Interface 00
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.iface01.am-lock-status
   :class: sensor-box

   **Description:** QSFP Alignment Marker Lock status for Interface 01
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.iface00.isPresent
   :class: sensor-box

   **Description:** Indicates the presence of QSFP Interface 00
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.iface01.isPresent
   :class: sensor-box

   **Description:** Indicates the presence of QSFP Interface 01
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.iface00.dhcp-enabled
   :class: sensor-box

   **Description:** Returns True/False if DHCP is enabled/disabled for data Interface 00
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.iface01.dhcp-enabled
   :class: sensor-box

   **Description:** Returns True/False if DHCP is enabled/disabled for data Interface 01
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.dhcp-handler.iface00.enabled
   :class: sensor-box

   **Description:** Reports the enabled state of DHCP handler for data Interface 00
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'DISABLED'
     - 'ENABLED'
     - 'NOT INSTALLED'
     - 'FAILED'

.. admonition:: edd.packetizer.dnet.dhcp-handler.iface01.enabled
   :class: sensor-box

   **Description:** Reports the enabled state of DHCP handler for data Interface 01
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'DISABLED'
     - 'ENABLED'
     - 'NOT INSTALLED'
     - 'FAILED'


Housekeeping
------------
.. admonition:: edd.packetizer.housekeeping.temperature.ffly0
   :class: sensor-box

   **Description:** Temperature of FireFly Module 0
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '60'
     - '0'
     - '70'

.. admonition:: edd.packetizer.housekeeping.temperature.ffly1
   :class: sensor-box

   **Description:** Temperature of FireFly Module 1
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '60'
     - '0'
     - '70'

.. admonition:: edd.packetizer.housekeeping.ffly0.lalarm
   :class: sensor-box

   **Description:** Reception Loss Alarm at FFly fiber connection
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.housekeeping.ffly1.lalarm
   :class: sensor-box

   **Description:** Reception Loss Alarm at FFly fiber connection
   
   **Unit:** NONE
   
   **Type:** discrete
   
   **Possible state / range:**
     - 'FALSE'
     - 'TRUE'
     - 'FAILED'

.. admonition:: edd.packetizer.housekeeping.temperature.board
   :class: sensor-box

   **Description:** Temperature of Packetizer PCB
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.packetizer.housekeeping.current.core
   :class: sensor-box

   **Description:** fpga core current
   
   **Unit:** Ampere
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '20'
     - '1'
     - '25'

.. admonition:: edd.packetizer.housekeeping.current.board
   :class: sensor-box

   **Description:** Board Main Current
   
   **Unit:** Ampere
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '7'
     - '1'
     - '8'

.. admonition:: edd.packetizer.housekeeping.temperature.fpga
   :class: sensor-box

   **Description:** FPGA Die Temperature
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.packetizer.housekeeping.temperature.reg1
   :class: sensor-box

   **Description:** Temperature of Voltage Regulator 1!
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '90'
     - '0'
     - '100'

.. admonition:: edd.packetizer.housekeeping.temperature.reg2
   :class: sensor-box

   **Description:** Temperature of Voltage Regulator 2!
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '90'
     - '0'
     - '100'

.. admonition:: edd.packetizer.housekeeping.voltage.int
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '1'
     - '0'
     - '1'

.. admonition:: edd.packetizer.housekeeping.voltage.aux
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

.. admonition:: edd.packetizer.housekeeping.voltage.6-12V
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '12'
     - '5'
     - '13'

.. admonition:: edd.packetizer.housekeeping.voltage.5V
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '4'
     - '5'
     - '4'
     - '5'

.. admonition:: edd.packetizer.housekeeping.voltage.3V3
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '3'
     - '3'
     - '2'
     - '3'

.. admonition:: edd.packetizer.housekeeping.voltage.3V3_fFly
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '3'
     - '3'
     - '2'
     - '3'

.. admonition:: edd.packetizer.housekeeping.voltage.1V8
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

.. admonition:: edd.packetizer.housekeeping.voltage.1V2_T
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '1'
     - '1'
     - '1'
     - '1'

.. admonition:: edd.packetizer.housekeeping.voltage.1V0_T
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '0'
     - '1'
     - '0'
     - '1'

.. admonition:: edd.packetizer.housekeeping.revolution.vent_board
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** U/min
   
   **Type:** float
   
   **Possible state / range:**
     - '1400'
     - '3500'
     - '1200'
     - '4000'

.. admonition:: edd.packetizer.housekeeping.revolution.vent_frame1
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** U/min
   
   **Type:** float
   
   **Possible state / range:**
     - '2100'
     - '3900'
     - '2000'
     - '4000'

.. admonition:: edd.packetizer.housekeeping.revolution.vent_frame2
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** U/min
   
   **Type:** float
   
   **Possible state / range:**
     - '2100'
     - '3900'
     - '2000'
     - '4000'

.. admonition:: edd.packetizer.housekeeping.voltage.fan
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** V
   
   **Type:** float
   
   **Possible state / range:**
     - '8'
     - '12'
     - '7'
     - '11'

.. admonition:: edd.packetizer.housekeeping.freq.uart-fm
   :class: sensor-box

   **Description:** Packetizer to Digitizer UART Modulation frequency
   
   **Unit:** MHz
   
   **Type:** float
   
   **Possible state / range:**
     - '95'
     - '105'
     - '90'
     - '110'

.. admonition:: edd.packetizer.housekeeping.freq.ffly-spare
   :class: sensor-box

   **Description:** none
   
   **Unit:** MHz
   
   **Type:** float
   
   **Possible state / range:**
     - '95'
     - '105'
     - '90'
     - '110'

.. admonition:: edd.packetizer.housekeeping.freq.refclk1
   :class: sensor-box

   **Description:** none
   
   **Unit:** MHz
   
   **Type:** float
   
   **Possible state / range:**
     - '207'
     - '229'
     - '196'
     - '240'

.. admonition:: edd.packetizer.housekeeping.freq.refclk2
   :class: sensor-box

   **Description:** none
   
   **Unit:** MHz
   
   **Type:** float
   
   **Possible state / range:**
     - '207'
     - '229'
     - '196'
     - '240'

.. admonition:: edd.packetizer.housekeeping.freq.adcs
   :class: sensor-box

   **Description:** none
   
   **Unit:** MHz
   
   **Type:** float
   
   **Possible state / range:**
     - '6650'
     - '7350'
     - '6300'
     - '7700'

.. admonition:: edd.packetizer.housekeeping.temperature.cpu0
   :class: sensor-box

   **Description:** NONE
   
   **Unit:** centigrade
   
   **Type:** float
   
   **Possible state / range:**
     - '5'
     - '75'
     - '0'
     - '85'

.. admonition:: edd.packetizer.housekeeping.os.localtime
   :class: sensor-box

   **Description:** The current used local system time as Unix epoch.
   
   **Unit:** s
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '999999999999'
     - '0'
     - '999999999999'

.. admonition:: edd.packetizer.housekeeping.os.mem.usage
   :class: sensor-box

   **Description:** The current used amount of memory.
   
   **Unit:** kB
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '2147483647'
     - '-1'
     - '2147483647'

.. admonition:: edd.packetizer.housekeeping.os.mem.total
   :class: sensor-box

   **Description:** The total available amount of system memory.
   
   **Unit:** kB
   
   **Type:** integer
   
   **Possible state / range:**
     - '0'
     - '2147483647'
     - '-1'
     - '2147483647'

Miscellaneous 
-------------

.. admonition:: edd.packetizer.serial-number
   :class: sensor-box

   **Description:** Packetizer Board ID
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.receptor-id
   :class: sensor-box

   **Description:** Antenna number
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.version.fpga
   :class: sensor-box

   **Description:** Packetizer Virtex-FPGA Build No
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.version.digitizer
   :class: sensor-box

   **Description:** Digitizer FPGA Build No
   
   **Unit:** NONE
   
   **Type:** integer

.. admonition:: edd.packetizer.version.packer.build
   :class: sensor-box

   **Description:** Packer build date in epoch
   
   **Unit:** Epoch
   
   **Type:** integer

.. admonition:: edd.packetizer.version.packer.svn
   :class: sensor-box

   **Description:** Packer SVN revision
   
   **Unit:** NONE
   
   **Type:** integer

