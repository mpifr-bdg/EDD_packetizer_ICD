.. toctree::
   :maxdepth: 2
   :caption: Contents:


Interface Control Document

Content
============

.. contents:: Table of Contents
   :depth: 3
   :local:

.. toctree::
   :maxdepth: 3

   interface_definition


.. toctree::
   :maxdepth: 3

   spead_parameters

.. toctree::
   :maxdepth: 3

   packetizer_katcp_interface

.. toctree::
   :maxdepth: 3

   operation_example



References
==========
1. http://casper.berkeley.edu/astrobaki/images/9/93/SPEADsignedRelease.pdf

2. https://casper.berkeley.edu/wiki/SPEAD

3. L-Band Digitizer CBF Interface Control Document (M1000-0001-053,
   Version: 3)

*LIST OF ABBREVIATIONS*

+-----------------------------+----------------------------------------+
| **ABBREVIATION**            |                                        |
+=============================+========================================+
| **Eff/Effelsberg**          | Effelsberg 100m Telescope              |
+-----------------------------+----------------------------------------+
| **GbE**                     | Gigabit Ethernet                       |
+-----------------------------+----------------------------------------+
| **ICD**                     | Interface Control Document             |
+-----------------------------+----------------------------------------+
| **IP**                      | Internet Protocol                      |
+-----------------------------+----------------------------------------+
| **FFT**                     | Fast Fourier Transform                 |
+-----------------------------+----------------------------------------+
| **LSB**                     | Least Significant Bit                  |
+-----------------------------+----------------------------------------+
| **MSB**                     | Most Significant Bit                   |
+-----------------------------+----------------------------------------+
| **PFB**                     | Polyphase Filter Bank                  |
+-----------------------------+----------------------------------------+
| **PPS**                     | Pulse Per Second                       |
+-----------------------------+----------------------------------------+
| **SPEAD**                   | Streaming Protocol for Exchanging      |
|                             | Astronomical Data                      |
+-----------------------------+----------------------------------------+
| **TBC**                     | To Be Confirmed                        |
+-----------------------------+----------------------------------------+
| **TBD**                     | To Be Determined                       |
+-----------------------------+----------------------------------------+
| **UBB**                     | Ultra-Broad Band Receiver              |
+-----------------------------+----------------------------------------+
| **UDP**                     | User Datagram Protocol                 |
+-----------------------------+----------------------------------------+
|                             |                                        |
+-----------------------------+----------------------------------------+

